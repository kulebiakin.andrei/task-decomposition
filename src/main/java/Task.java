public class Task {

    public static void main(String[] args) {
        System.out.println("Greatest Common Divisor: " + findGreatestCommonDivisor(30, 18));
        System.out.println("Least Common Multiple: " + findLeastCommonMultiple(30, 18));
        System.out.println("Greatest Common Divisor for 4 numbers: "
                + findGreatestCommonDivisor(30, 18, 144, 345));
        System.out.println("Least Common Multiple for 3 numbers: "
                + findLeastCommonMultiple(30, 18, 3234));
        System.out.println("Sum of factorials of all odd numbers from 1 to 9"
                + calculateSumOfFactorialsForAllOddNumbers(1, 9));
        System.out.println("Second largest number: " + findSecondLargestNumber(new int[]{0, 12, 145, 236, 745}));

    }

    /**
     * Task 1. Write a method(s) for finding the greatest common divisor
     * and the smallest common multiple of two natural numbers:
     */
    private static int findLeastCommonMultiple(int first, int second) {
        return (first * second) / findGreatestCommonDivisor(first, second);
    }

    private static int findGreatestCommonDivisor(int first, int second) {
        int tempFirst = first;
        int tempSecond = second;

        while (tempFirst != 0 && tempSecond != 0) {
            if (tempFirst > tempSecond) {
                tempFirst = tempFirst % tempSecond;
            } else {
                tempSecond = tempSecond % tempFirst;
            }
        }

        return tempFirst + tempSecond;
    }

    /**
     * 2. Write a method(s) for finding the greatest common divisor of four natural numbers.
     */
    private static int findGreatestCommonDivisor(int first, int second,
                                                 int third, int fourth) {
        int gcd = findGreatestCommonDivisor(first, second);
        gcd = findGreatestCommonDivisor(gcd, third);
        gcd = findGreatestCommonDivisor(gcd, fourth);
        return gcd;
    }

    /**
     * 3. Write a method(s) for finding the smallest common multiple of three natural numbers.
     */
    private static int findLeastCommonMultiple(int first, int second, int third) {
        int lcm = findLeastCommonMultiple(first, second);
        lcm = findLeastCommonMultiple(lcm, third);
        return lcm;
    }

    /**
     * 4. Write a method(s) for calculating the sum of factorials of all odd numbers from 1 to 9.
     */
    private static long calculateSumOfFactorialsForAllOddNumbers(int first, int second) {
        long result = 0;

        for (int i = first; i <= second; i++) {
            if (isOdd(i)) {
                result = result + calculateFactorial(i);
            }
        }

        return result;
    }

    private static boolean isOdd(int number) {
        return number % 2 != 0;
    }

    private static long calculateFactorial(int number) {
        long factorial = 1;

        for (int i = 2; i <= number; i++) {
            factorial = factorial * i;
        }

        return factorial;
    }

    /**
     * 5. Create a program that finds the second-largest number in the array A[N]
     * (print out the number that is smaller than the maximum element of the array,
     * but larger than all other elements).
     */
    private static int findSecondLargestNumber(int[] array) {
        int largest = array[0];
        int secondLargest = array[0];

        for (int number : array) {
            if (number > largest) {
                secondLargest = largest;
                largest = number;
            } else if (number > secondLargest) {
                secondLargest = number;
            }
        }

        return secondLargest;
    }
}
